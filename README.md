# Setting up netbeans #
1. Install the latest netbeans IDE
2. In netbeans install the [Gradle plugin](http://plugins.netbeans.org/plugin/44510/gradle-support)
3. Download the repository as a zip or use git clone to download the repository to your local system.
4. In Netbeans go to *file > open project* and navigate to the root of the project(repository), the project will have a gradle icon.
5. Select Open project and the project will open in netbeans

# Setting up the database #
1. Download mySQL (mariaDB) on your system (this step is system dependant)
2. Login to mySQL using ```mysql -u root -p``` and press enter when requested to enter a password (Default root password is empty)
3. Then type ```source <creation file>``` and press enter.
4. If everything executes successfully mySQL should create a new database with all the required tables.