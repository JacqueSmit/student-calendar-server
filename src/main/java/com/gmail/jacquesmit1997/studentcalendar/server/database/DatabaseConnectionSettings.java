/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database;

/**
 * Represents the database connection settings used to successfully connect to a
 * database.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/28.
 * @see DatabaseHelper#open(DatabaseConnectionSettings)
 */
public class DatabaseConnectionSettings {
    /**
     * The user that will be used to connect to the database.
     */
    private String username = DatabaseContract.USERNAME;

    /**
     * The password to authenticate the user.
     */
    private String password = DatabaseContract.USER_PASSWORD;

    /**
     * The name of the database.
     */
    private String databaseName = DatabaseContract.DATABASE_NAME;

    /**
     * The host that's running the database.
     */
    private String host = "localhost";

    /**
     * The port the database is running on.
     */
    private int port = 3306;

    /**
     * Creates an empty <code>DatabaseConnectionSettings</code> object containing all the default settings.
     */
    public DatabaseConnectionSettings() {
    }

    public static void main(String[] args) {
        DatabaseConnectionSettings settings = new DatabaseConnectionSettings();

        System.out.println(settings.getUrl());
    }

    /**
     * Creates the url that can be used to access the database related to the settings in this object.
     *
     * @return a URL pointing to the database in this object.
     */
    public String getUrl() {
        return "jdbc:mysql://" +
                host + ":" +
                port + "/" +
                databaseName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
