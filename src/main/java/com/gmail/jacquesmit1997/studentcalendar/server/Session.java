/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server;

import com.gmail.jacquesmit1997.studentcalendar.server.database.tables.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Represents a session between a connected client and the server.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/05/02.
 */
public class Session {
    /**
     * Whether or not the client is authenticated or not.
     * Authentication should be the first step when a client connects and the session
     * should be revoked if authentication was unsuccessful.
     */
    private boolean authenticated;

    /**
     * The student that is connected to this session.
     */
    private Student student;

    /**
     * The socket connecting the client to the server.
     */
    private Socket socket;

    /**
     * The input stream from the client.
     */
    private BufferedReader in;

    /**
     * The output stream to the client.
     */
    private PrintWriter out;

    /**
     * Creates a new <code>Session</code> object based around the given <code>socket</code>.
     *
     * @param socket The socket the session will revolve around.
     */
    public Session(Socket socket) throws IOException {
        this.socket = socket;

        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new PrintWriter(socket.getOutputStream(), true);

        System.out.println(socket.getInetAddress().toString() + " connected");

        listen();
    }

    /**
     * Listens for requests coming from the client.
     */
    private void listen() {
        new Thread(() -> {
            try {
                while (socket != null) {
                    // Retrieve XML string from client and process.
                    String xmlStr = in.readLine();

                    // Process xml String by checking which method to call, call the respective method and return result.
                    // TODO 2017/05/04: Process XML String and call respective method.
                    out.println(xmlStr);
                }
            } catch (IOException ioe) {
                System.err.println(socket.getInetAddress().toString() + " " + ioe.getMessage());
                socket = null;
            } catch (Exception e) {
                System.err.println(socket.getInetAddress().toString() + " disconnected");
                socket = null;
            }
        }).start();
    }

}
