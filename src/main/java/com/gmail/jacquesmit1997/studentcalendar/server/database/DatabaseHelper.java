/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database;

import com.gmail.jacquesmit1997.studentcalendar.server.database.tables.Course;
import com.gmail.jacquesmit1997.studentcalendar.server.database.tables.University;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Database connection class used to read and write information to the database.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/27.
 */
public class DatabaseHelper implements AutoCloseable {
    public static String NO_CONNECTION_MESSAGE = "No database connection";
    /**
     * The only <code>DatabaseHelper</code> object for a runtime.
     */
    private static DatabaseHelper helper = new DatabaseHelper();
    /**
     * The actual connection to the database.
     */
    private Connection conn;

    /**
     * Private constructor to prevent instantiation of this class.
     */
    private DatabaseHelper() {
    }

    /**
     * Returns the only database helper object available for a runtime.
     *
     * @return The <code>DatabaseHelper</code> object.
     */
    public static DatabaseHelper getDatabaseHelper() {
        return helper;
    }

    // ====================================== Actual database helper methods ===========================================

    // TODO 2017/04/28: Complete this class

    /**
     * Opens a connection to the given database if, and only if a connection does not already exist.
     * If a pre-existing connection is found then this method will execute without any changes.
     *
     * @throws SQLException if a database connection error occurs.
     */
    public void open(DatabaseConnectionSettings settings) throws SQLException {
        if (!isConnected()) {
            conn = DriverManager.getConnection(settings.getUrl(), settings.getUsername(), settings.getPassword());
        }
    }

    /**
     * Checks the connection state to a database.
     *
     * @return <code>true</code> if and only if an active connection exists, <code>false</code> if not.
     */
    public boolean isConnected() {
        try {
            return conn != null && !conn.isClosed();
        } catch (SQLException sqle) {
            return false;
        }
    }

    /**
     * Adds a new {@link University} to the database.<p>
     * <b>Note:</b> All the courses related to this university <b>WILL NOT</b> be added to the
     * database. In order to add the courses in this {@link University} object see
     * {@link #addCourse(Course, University)}.
     *
     * @param university The university to add to the database.
     * @throws SQLException if a database connection error occurs or if there is no database connection.
     */
    public void addUniversity(University university) throws SQLException {
        if (isConnected()) {
            String insertStatement = "INSERT INTO " + DatabaseContract.University.TABLE_NAME
                    + " VALUES (?)";

            try (PreparedStatement ps = conn.prepareStatement(insertStatement)) {
                ps.setString(1, university.getName());

                ps.executeUpdate();
            }
        } else {
            throw new SQLException(NO_CONNECTION_MESSAGE);
        }
    }

    /**
     * Adds a new course to the database.
     * <b>Note:</b> The <code>ID</code> of <code>university</code> needs to be present and correct
     * in order for this method to execute successfully.
     *
     * @param course     The course to be added to the database.
     * @param university The university the course is related to.
     * @throws SQLException If a database connection error occurs or if there is no database connection.
     */
    // TODO 2017/04/28: Add @see to the getUniversity() method
    public void addCourse(Course course, University university) throws SQLException {
        String insertStatement = "INSERT INTO " + DatabaseContract.Course.COLUMN_NAME
                + " VALUES (?, ?)";
        if (isConnected()) {
            try (PreparedStatement ps = conn.prepareStatement(insertStatement)) {
                ps.setString(1, course.getName());
                ps.setInt(2, university.getId());

                ps.executeUpdate();
            }
        } else {
            throw new SQLException(NO_CONNECTION_MESSAGE);
        }
    }

    /**
     * Closes the connection to the database (if any).
     * If there was no connection to the database ({@link #open(DatabaseConnectionSettings)}
     * not called before) then this method will
     * execute without changing anything.
     *
     * @throws SQLException if a database connection error occurs.
     */
    @Override
    public void close() throws SQLException {
        if (isConnected()) {
            conn.close();
            conn = null;
        }
    }
}
