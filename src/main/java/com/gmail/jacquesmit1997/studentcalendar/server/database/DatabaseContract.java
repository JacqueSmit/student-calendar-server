/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database;

/**
 * Allows a single reference point to all the tables and columns in the database.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/27.
 */
public class DatabaseContract {
    /**
     * The current version of the database.
     */
    public static final int DATABASE_VERSION = 1;

    /**
     * The database name.
     */
    public static final String DATABASE_NAME = "studentCalendar";

    /**
     * The mySQL user that will be used to access the database.
     */
    public static final String USERNAME = "calendarUser";

    /**
     * The password for user used to connect to the database.
     */
    public static final String USER_PASSWORD = "calendar123";

    /**
     * Private constructor to prevent instantiation of the class.
     */
    private DatabaseContract() {
    }

    /**
     * The course table found in the database.
     */
    public static final class Course {
        /**
         * The name of the table.
         */
        public static final String TABLE_NAME = "course";
        /**
         * <code>Primary key</code>. The primary key for each course that will auto increment
         * with every new course added.
         */
        public static final String COLUMN_ID = "id";

        /**
         * The name of the course.
         * <code>Not null</code>
         */
        public static final String COLUMN_NAME = "name";

        /**
         * <b>Foreign key</b>. The relation between a course and a university.
         */
        public static final String COLUMN_UNIVERSITY = "university";
    }

    /**
     * The <code>file</code> table found in the database.
     * Files are attached to <code>Notes</code> enabling other students to access the relevant materials.
     */
    public static final class File {
        /**
         * The name of the table.
         */
        public static final String TABLE_NAME = "file";

        /**
         * <b>Primary key</b>. The primary key for each file that will auto increment
         * with every new file added.
         */
        public static final String COLUMN_ID = "id";

        /**
         * The filename of the specific file (including extensions).
         */
        public static final String COLUMN_FILENAME = "filename";

        /**
         * <b>Foreign key</b>. The relation between a file and a note.
         */
        public static final String COLUMN_NOTE = "note";

        /**
         * The path (including filename) leading to the file on the servers' directory
         * structure.
         */
        public static final String COLUMN_PATH = "path";
    }

    /**
     * The <code>Note</code> table found on the database.
     */
    public static final class Note {
        /**
         * The name of the table.
         */
        public static final String TABLE_NAME = "note";

        /**
         * <b>Primary key</b>. The primary key for each note that will auto increment with
         * every new <code>note</code> added to the database.
         */
        public static final String COLUMN_ID = "id";

        /**
         * <code>Foreign key</code>. The relation between a <code>note</code> and the <code>course</code>
         * it was written for.
         */
        public static final String COLUMN_COURSE = "course";

        /**
         * A description about the note.
         */
        public static final String COLUMN_DESCRIPTION = "description";

        /**
         * The date the <code>note</code> was set to be due.
         */
        public static final String COLUMN_DUE_DATE = "dueDate";

        /**
         * The date and time the note was posted.
         */
        public static final String COLUMN_POST_DATE = "postDate";

        /**
         * <code>Foreign key</code>. The relation to the student who created the note.
         */
        public static final String COLUMN_STUDENT = "student";

        /**
         * The title of the note.
         */
        public static final String COLUMN_TITLE = "title";
    }

    /**
     * The <b>student</b> table found in the database.
     */
    public static final class Student {
        /**
         * The name of the table.
         */
        public static final String TABLE_NAME = "student";

        /**
         * <b>Primary key</b>. The primary key for the table that will auto increment
         * each time a new student has been added to the database.
         */
        public static final String COLUMN_ID = "id";

        /**
         * <b>Foreign key</b>. The course the student is currently enrolled with.
         */
        public static final String COLUMN_COURSE = "course";

        /**
         * The name of the student.
         */
        public static final String COLUMN_NAME = "name";

        /**
         * The surname of the student (<b>Optional</b>).
         */
        public static final String COLUMN_SURNAME = "surname";

        /**
         * The password of the student.
         */
        public static final String COLUMN_PASSWORD = "password";

        /**
         * The amount of times the student has been reported for spam.
         */
        public static final String COLUMN_SPAMCOUNT = "spamcount";
    }

    /**
     * The <code>university</code> table that can be found in the database.
     */
    public static final class University {

        /**
         * The name of the table.
         */
        public static final String TABLE_NAME = "university";

        /**
         * The primary key for the table that will auto increment for each university added to the database.
         */
        public static final String COLUMN_ID = "id";

        /**
         * The name of the university.
         */
        public static final String COLUMN_NAME = "name";
    }

    /**
     * The <code>vote</code> table found in the database.
     */
    public static final class Vote {
        /**
         * The name of the table.
         */
        public static final String TABLE_NAME = "vote";

        /**
         * <b>Primary key</b>, <b>Foreign key</b>. The note the vote was made for.
         */
        public static final String COLUMN_NOTE = "note";

        /**
         * <b>Primary key</b>, <b>Foreign key</b>. The student that made the vote.
         */
        public static final String COLUMN_STUDENT = "student";

        /**
         * <b>Foreign key</b>. The type of vote the student made for a note.
         */
        public static final String COLUMN_TYPE = "type";
    }

    /**
     * The <code>vote_type</code> table found in the database containing the types of votes users can make.
     */
    public static final class VoteType {

        /**
         * <b>Primary key</b>. The primaryu key for the table.
         */
        public static final String COLUMN_ID = "id";

        /**
         * The type of vote.
         */
        public static final String COLUMN_TYPE = "type";
    }
}
