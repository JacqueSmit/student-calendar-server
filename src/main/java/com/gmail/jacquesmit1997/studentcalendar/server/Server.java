/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;

/**
 * This class will listen on the given port (or default 8080) and accept any connections
 * from clients.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/27.
 */
public class Server implements AutoCloseable {

    /**
     * The port the server will run on.
     */
    private int port;

    /**
     * The socket that will accept client connections.
     */
    private ServerSocket serverSocket;

    /**
     * Creates a new empty <code>Server</code> object that will run on port 8080.
     */
    public Server() {
        this(8080);
    }

    /**
     * Creates a new <code>Server</code> object that will listen on the given port.
     *
     * @param port The port the server will listen for incoming connections.
     */
    public Server(int port) {
        this.port = port;
    }

    /**
     * Main entry point for the server.
     *
     * @param args <code>args[0]</code> the port the server should run on (Optional).
     */
    public static void main(String[] args) {
        Server server;

        if (args.length == 1) {
            try {
                int port = Integer.parseInt(args[0]);
                server = new Server(port);
            } catch (NumberFormatException ioe) {
                server = null; // Required to remove compile time warning
                System.err.println("Usage: Server <port>");
                System.exit(1);
            }
        } else {
            server = new Server();
        }

        try {
            server.open();
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        System.out.println("Starting server...");
    }

    /**
     * Opens the server port and starts listening for incomming connections.
     *
     * @throws IOException if the server could not be started.
     */
    public void open() throws IOException {
        if (!isRunning()) {
            this.serverSocket = new ServerSocket(port);
            this.accept();
        }
    }

    /**
     * Listens for and accepts incoming client connections.
     */
    private void accept() {
        try {
            serverSocket.setSoTimeout(10000);
        } catch (SocketException ignored) {
            // Should not be an issue.
        }
        System.out.println("Accepting incoming connections...");

        while (isRunning()) {
            try {
                new Session(serverSocket.accept());
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * Checks the running state of the server.
     *
     * @return <code>true</code> if, and only if the server is running, <code>false</code> otherwise.
     */
    public boolean isRunning() {
        return !(serverSocket == null || serverSocket.isClosed());
    }

    /**
     * Closes the server and all socket associated with the server.
     *
     * @throws IOException If the server could not be closed.
     */
    @Override
    public void close() throws IOException {
        if (isRunning()) {
            serverSocket.close();
            serverSocket = null;
        }
    }
}
