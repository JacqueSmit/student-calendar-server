/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database.tables;

import nu.xom.Element;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a <code>Note</code> object that can be found in the system.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/28.
 */
public class Note implements XMLParseable {
    /**
     * The unique ID of the note.
     */
    private int id;

    /**
     * The title of the note.
     */
    private String title;

    /**
     * The description of the note.
     */
    private String description;

    /**
     * The date the note was posted.
     */
    private Date postDate;

    /**
     * The date the note is due.
     */
    private Date dueDate;

    /**
     * The course the note is relevant to.
     */
    private Course course;

    /**
     * The student who created the note.
     */
    private Student student;

    /**
     * The amount of votes received.
     */
    private List<Vote> votes;

    /**
     * The files attached to this note.
     */
    private List<NoteFile> files;


    /**
     * Creates a new empty <code>Note</code> object.
     */
    public Note() {
        this.postDate = new Date(System.currentTimeMillis());
        this.votes = new ArrayList<>();
        this.files = new ArrayList<>();
    }

    /**
     * Creates a new <code>Note</code> object with all the given information.
     *
     * @param id          The unique ID of the note.
     * @param title       The title of the note.
     * @param description The description of the note.
     * @param postDate    The date the note was posted.
     * @param dueDate     The date the note is due.
     * @param course      The course the note is relevant to.
     * @param student     The student who created the note.
     * @param votes       The list of votes for this note.
     * @param files       The files attached to this note.
     */
    public Note(int id,
                String title,
                String description,
                Date postDate,
                Date dueDate,
                Course course,
                Student student,
                List<Vote> votes,
                List<NoteFile> files) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.postDate = postDate;
        this.dueDate = dueDate;
        this.course = course;
        this.student = student;
        this.votes = votes;
        this.files = files;
    }

    /**
     * Returns the XML representation of a class contained within the root {@link Element}.
     *
     * @return The root Element containing all other elements.
     */
    @Override
    public Element toElement() {
        Element root = new Element("note");

        Element eId = new Element("id");
        eId.appendChild(Integer.toString(id));
        root.appendChild(eId);

        Element eTitle = new Element("title");
        eTitle.appendChild(title);
        root.appendChild(eTitle);

        Element eDescription = new Element("description");
        eDescription.appendChild(description);
        root.appendChild(eDescription);

        Element ePostDate = new Element("post_date");
        ePostDate.appendChild(Long.toString(postDate.getTime()));
        root.appendChild(ePostDate);

        Element eDueDate = new Element("due_date");
        eDueDate.appendChild(Long.toString(postDate.getTime()));
        root.appendChild(eDueDate);

        Element eStudent;

        if (student != null) {
            eStudent = student.toElement();
        } else {
            eStudent = new Element("student");
        }
        root.appendChild(eStudent);

        Element eVotes = new Element("votes");

        for (Vote vote : votes) {
            eVotes.appendChild(vote.toElement());
        }

        root.appendChild(eVotes);

        Element eFiles = new Element("files");

        for (NoteFile file : files) {
            eFiles.appendChild(file.toElement());
        }
        
        root.appendChild(eFiles);

        return root;
    }

    public void addVote(Vote vote) {
        votes.add(vote);
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public List<NoteFile> getFiles() {
        return files;
    }

    public void setFiles(List<NoteFile> files) {
        this.files = files;
    }

    public void addFile(NoteFile file) {
        files.add(file);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
