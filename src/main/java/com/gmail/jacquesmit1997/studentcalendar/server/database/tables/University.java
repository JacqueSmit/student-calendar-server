/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database.tables;

import nu.xom.Element;

import java.util.List;

/**
 * Represents a <code>University</code> in the system containing different courses.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/28.
 */
public class University implements XMLParseable {
    /**
     * The unique identifier of the university.
     */
    private int id;

    /**
     * All the courses registered with the university.
     */
    private List<Course> courses;

    /**
     * The name of the university.
     */
    private String name;

    /**
     * Creates a new <code>University</code> object.
     */
    public University() {
    }

    /**
     * Creates a new <code>University</code> object with the given <code>id</code> and <code>courses</code>
     *
     * @param id      The unique ID of the university.
     * @param courses The courses registered to this university.
     */
    public University(int id, List<Course> courses) {
        this.id = id;
        this.courses = courses;
    }

    /**
     * Returns the XML representation of a class contained within the root {@link Element}.
     *
     * @return The root Element containing all other elements.
     */
    @Override
    public Element toElement() {
        Element root = new Element("university");

        Element eId = new Element("id");
        eId.appendChild(Integer.toString(id));
        root.appendChild(eId);

        Element eName = new Element("name");
        eName.appendChild(eName);
        root.appendChild(eName);

        return root;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "University{" +
                "id=" + id +
                ", courses=" + courses +
                '}';
    }
}
