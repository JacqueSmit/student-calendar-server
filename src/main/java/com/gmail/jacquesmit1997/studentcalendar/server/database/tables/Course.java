/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database.tables;

import nu.xom.Element;

/**
 * Represents a <code>Course</code> found in the system with an <code>ID</code> and a <code>Name</code>
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/28.
 */
public class Course implements XMLParseable {
    /**
     * The ID of the course.
     */
    private int id;

    /**
     * The name of the course.
     */
    private String name;

    public Course() {
    }

    /**
     * Creates a new <code>Course</code> object with the given <code>id</code> and <code>name</code>.
     *
     * @param id   The ID of the course.
     * @param name The name of the course.
     */
    public Course(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Returns the XML representation of a class contained within the root {@link Element}.
     *
     * @return The root Element of this object containing all other {@link Element elements}.
     */
    @Override
    public Element toElement() {
        Element root = new Element("course");

        Element eId = new Element("id");
        eId.appendChild(Integer.toString(id));
        root.appendChild(eId);

        Element eName = new Element("name");
        eName.appendChild(name);
        root.appendChild(eName);

        return root;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
