/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database.tables;

import nu.xom.Element;

/**
 * Represents a student in the system.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/28.
 */
public class Student implements XMLParseable {
    /**
     * The unique ID of the student.
     */
    private int id;

    /**
     * The name of the student.
     */
    private String name;

    /**
     * The surname of the student.
     */
    private String surname;

    /**
     * The password of the student.
     * <b>Note:</b> Due to security reasons the password will not be included in the {@link #toElement()} methods'
     * result.
     */
    private String password;

    /**
     * The amount of spam the user has been reported for.
     */
    private int spamcount = 0;

    /**
     * The course the student is enrolled with.
     */
    private Course course;

    /**
     * Creates a new <code>Student</code> object.
     */
    public Student() {
    }

    /**
     * Creates a new <code>Student</code> object with all the given information.
     *
     * @param id        The unique ID of the student.
     * @param name      The name of the student.
     * @param surname   The surname of the student.
     * @param password  The password of the student.
     * @param spamcount The spamcount of the student.
     * @param course    The course the student is enrolled with.
     */
    public Student(int id, String name, String surname, String password, int spamcount, Course course) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.spamcount = spamcount;
        this.course = course;
    }

    /**
     * Returns the XML representation of a class contained within the root {@link Element}.
     *
     * @return The root Element containing all other elements.
     */
    @Override
    public Element toElement() {
        Element root = new Element("student");

        Element eId = new Element("id");
        eId.appendChild(Integer.toString(id));
        root.appendChild(eId);

        Element eName = new Element("name");
        eName.appendChild(name);
        root.appendChild(eName);

        Element eSurname = new Element("surname");
        eSurname.appendChild(surname);
        root.appendChild(surname);

        Element eSpamCount = new Element("spam_count");
        eSpamCount.appendChild(Integer.toString(spamcount));

        Element eCourse;

        if (course != null) {
            eCourse = course.toElement();
        } else {
            eCourse = new Element("course");
        }

        root.appendChild(eCourse);

        return root;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSpamcount() {
        return spamcount;
    }

    public void setSpamcount(int spamcount) {
        this.spamcount = spamcount;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", password='" + password + '\'' +
                ", spamcount=" + spamcount +
                ", course=" + course +
                '}';
    }
}
