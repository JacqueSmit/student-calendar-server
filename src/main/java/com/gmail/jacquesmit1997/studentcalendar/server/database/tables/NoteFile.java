/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database.tables;

import nu.xom.Element;

import java.io.File;

/**
 * Represents a file that can be sent over a network and contains a unique database ID.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/28.
 */
public class NoteFile extends File implements XMLParseable {
    // TODO 2017/04/28: Complete this class!!!
    /**
     * The unique ID of the file.
     */
    private int id;

    /**
     * Creates a new <code>File</code> instance by converting the given
     * pathname string into an abstract pathname.  If the given string is
     * the empty string, then the result is the empty abstract pathname.
     *
     * @param pathname A pathname string
     * @throws NullPointerException If the <code>pathname</code> argument is <code>null</code>
     */
    public NoteFile(String pathname) {
        super(pathname);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the XML representation of a class contained within the root {@link Element}.
     *
     * @return The root Element of an XML parseable object.
     */
    @Override
    public Element toElement() {
        Element root = new Element("file");

        Element eId = new Element("id");
        eId.appendChild(Integer.toString(id));
        root.appendChild(eId);

        Element eName = new Element("name");
        eName.appendChild(this.getName());
        root.appendChild(eName);

        return root;
    }
}
