/*
 * server
 * Copyright (C) 2017  Jacque Smit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.jacquesmit1997.studentcalendar.server.database.tables;

import nu.xom.Element;

/**
 * Represents a vote that a student will be able to make for a specific {@link Note}.
 *
 * @author Jacque Smit
 * @version 1.0, 2017/04/28.
 */
public class Vote implements XMLParseable {
    /**
     * The name of the student.
     */
    private String studentName;

    /**
     * The surname of the student.
     */
    private String studentSurname;

    /**
     * The type of the vote.
     */
    private VoteType type;

    /**
     * Creates a new <code>Vote</code> object without a {@link VoteType}.
     * <b>Default vote:</b> {@link VoteType#UNDECIDED}
     *
     * @param studentName    The name of the student who made the vote.
     * @param studentSurname The surname of the student who made the vote.
     */
    public Vote(String studentName, String studentSurname) {
        this.studentName = studentName;
        this.studentSurname = studentSurname;
        this.type = VoteType.UNDECIDED;
    }

    /**
     * Creates a new <code>Vote</code> object.
     *
     * @param studentName    The name of the student who made the vote.
     * @param studentSurname The surname of the student who made the vote.
     * @param type           The type of vote.
     */
    public Vote(String studentName, String studentSurname, VoteType type) {
        this.studentName = studentName;
        this.studentSurname = studentSurname;
        this.type = type;
    }

    /**
     * Returns the XML representation of a class contained within the root {@link Element}.
     *
     * @return The root Element of an XML parseable object.
     */
    @Override
    public Element toElement() {
        Element root = new Element("vote");

        Element eStudent = new Element("student");

        Element eName = new Element("name");
        eName.appendChild(studentName);
        eStudent.appendChild(eName);

        Element eSurname = new Element("surname");
        eSurname.appendChild(studentSurname);
        eStudent.appendChild(eSurname);

        root.appendChild(eStudent);

        Element eType = new Element("type");
        eType.appendChild(type.toString());
        root.appendChild(root);

        return root;
    }

    public void setStudent(Student student) {
        this.studentName = student.getName();
        this.studentSurname = student.getSurname();
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSurname() {
        return studentSurname;
    }

    public void setStudentSurname(String studentSurname) {
        this.studentSurname = studentSurname;
    }

    public VoteType getType() {
        return type;
    }

    public void setType(VoteType type) {
        this.type = type;
    }
}
