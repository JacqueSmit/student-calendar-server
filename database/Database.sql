CREATE DATABASE studentCalendar;

-- Create user and privileges
CREATE USER 'calendarUser'@'localhost' IDENTIFIED BY 'calendar123';
GRANT ALL PRIVILEGES ON studentCalendar.* TO 'calendarUser'@'localhost';
FLUSH PRIVILEGES;

-- Create database tables
USE studentCalendar;

CREATE TABLE `studentCalendar`.`university` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `studentCalendar`.`course` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `university` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_course_university_idx` (`university` ASC),
  CONSTRAINT `fk_course_university`
    FOREIGN KEY (`university`)
    REFERENCES `studentCalendar`.`university` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);

    CREATE TABLE `studentCalendar`.`student` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `surname` VARCHAR(50) NULL,
  `password` VARCHAR(120) NOT NULL,
  `spamcount` INT NOT NULL DEFAULT 0,
  `course` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_student_course_idx` (`course` ASC),
  CONSTRAINT `fk_student_course`
    FOREIGN KEY (`course`)
    REFERENCES `studentCalendar`.`course` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);

CREATE TABLE `studentCalendar`.`note` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(70) NOT NULL,
  `description` TEXT NULL,
  `postDate` DATETIME NOT NULL,
  `dueDate` DATE NOT NULL,
  `course` INT NOT NULL,
  `student` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_note_course_idx` (`course` ASC),
  INDEX `fk_note_student_idx` (`student` ASC),
  CONSTRAINT `fk_note_course`
    FOREIGN KEY (`course`)
    REFERENCES `studentCalendar`.`course` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_note_student`
    FOREIGN KEY (`student`)
    REFERENCES `studentCalendar`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `studentCalendar`.`file` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` TEXT NOT NULL,
  `filename` VARCHAR(100) NOT NULL,
  `note` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_file_note_idx` (`note` ASC),
  CONSTRAINT `fk_file_note`
    FOREIGN KEY (`note`)
    REFERENCES `studentCalendar`.`note` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE `studentCalendar`.`vote_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `type_UNIQUE` (`type` ASC));

CREATE TABLE `studentCalendar`.`vote` (
  `student` INT NOT NULL,
  `note` INT NOT NULL,
  `type` INT NOT NULL,
  PRIMARY KEY (`student`, `note`),
  INDEX `fk_vote_note_idx` (`note` ASC),
  CONSTRAINT `fk_vote_student`
    FOREIGN KEY (`student`)
    REFERENCES `studentCalendar`.`student` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vote_note`
    FOREIGN KEY (`note`)
    REFERENCES `studentCalendar`.`note` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
